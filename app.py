from flask import Flask, render_template
from flask_assets import Environment, Bundle 

app = Flask(__name__)

assets = Environment(app)
assets.url = app.static_url_path
scss = Bundle('scss/style.scss', filters='pyscss', output='css/style.css')
assets.register('scss_all', scss)


@app.route("/")
def hello():
    return render_template('test.html')                                                    


if __name__ == '__main__':
    app.run(debug=True)

